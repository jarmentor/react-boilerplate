const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    output: {
      filename: '[name].bundle.[chunkhash].js',
    },
    plugins: [
        new UglifyJSPlugin(
        {
        	sourceMap: true
        }),
        new webpack.DefinePlugin({
        	'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.SplitChunksPlugin({
            name: "manifest",
            minChunks: Infinity
        })
    ]

});