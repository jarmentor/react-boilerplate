const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");

module.exports = {
  entry: path.resolve(__dirname, "src/index.js"),
  resolve: {
    modules: [path.resolve(__dirname, "src"), "node_modules"]
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "build")
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
            loader: "babel-loader",
            options: {
              babelrc: false,
              presets: ["env", "react", "flow", "stage-0", "es2015"]
            }
      },
      {
        test: /\.(s?css)$/,
        use: [
          {
            loader: "style-loader" // inject CSS to page
          },
          {
            loader: "css-loader" // translates CSS into CommonJS modules
          },
          {
            loader: "postcss-loader", // Run post css actions
            options: {
              plugins: function() {
                // post css plugins, can be exported to postcss.config.js
                return [require("precss"), require("autoprefixer")];
              }
            }
          },
          {
            loader: "sass-loader" // compiles Sass to CSS
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,

        use: ["file-loader"]
      }
    ]
  },
  plugins: [
    new ManifestPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "public/index.html")
    }),
    new CleanWebpackPlugin(["build"], {
      verbose: false
    }),
    new webpack.optimize.SplitChunksPlugin({
      name: "common"
    }),
    new webpack.optimize.SplitChunksPlugin({
      name: "vendor",
      minChunks: function(module) {
        return module.context && module.context.includes("node_modules");
      }
    })
  ]
};
