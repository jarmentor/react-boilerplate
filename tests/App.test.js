import { shallow } from 'enzyme'
import React from 'react'
import renderer from 'react-test-renderer'

import App from '../src/App.js'

describe('With Enzyme', () => {
  it('App shows "Blue" and Red', () => {
    const app = shallow(<App />)

    expect(app.find('h1').text()).toEqual('Blue')
    expect(app.find('h5').text()).toEqual('red')
  })
})

describe('With Snapshot Testing', () => {
  it('App shows "Blue" and Red', () => {
    const component = renderer.create(<App />)
    const tree = component.toJSON()
    expect(tree).toMatchSnapshot()
  })
})